<?php 
/*----------------------------------------------------------------*\

	ERROR / NO PAGE FOUND

\*----------------------------------------------------------------*/
?>


<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/elements/navigation-mobile'); ?>

<main id="main-content">
	<article>
		<section class="wysiwyg-block">
			<h1>Not Found</h1>
			<p>This page seems to be lost in the basement of our warehouse, but no one goes down there. It's scary.</p>
			<div class="buttons">
				<a class="button is-primary" href="/">Return to the Homepage</a>
				<a class="button is-ghost" href="/">Contact Support</a>
			</div>
		</section>
	</article>
</main>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>