var $ = jQuery;

$(document).ready(function () {
	/*----------------------------------------------------------------*\
  	ENTRANCE ANIMATIONS
	\*----------------------------------------------------------------*/
	emergence.init({
		offsetTop: 20,
		offsetRight: 20,
		offsetBottom: 20,
		offsetLeft: 20,
	});
	/*----------------------------------------------------------------*\
		FILE UPLOAD
	\*----------------------------------------------------------------*/
	if ($('input[type="file"]').length > 0) {
		var fileInput = document.querySelector("input[type='file']");
		var button = document.querySelector("input[type='file']+span");
		fileInput.addEventListener("change", function (event) {
			button.innerHTML = this.value;
			fileInput.classList.add("file-uploaded");
		});
	}
	/*----------------------------------------------------------------*\
  	LOGIC FOR LOAD MORE BUTTON
	\*----------------------------------------------------------------*/
	if ($('.next.page-numbers').length) {
		$('.load-more').css('display', 'table');
	}
	/*----------------------------------------------------------------*\
		INFINITE SCROLL INIT
	\*----------------------------------------------------------------*/
	$('.archive ul.products').infiniteScroll({
		path: '.next.page-numbers',
		append: 'li.product',
		checkLastPage: true,
		history: false,
		status: '.page-load-status',
	});
	/*----------------------------------------------------------------*\
		PRODUCT INFINITE SCROLL INIT
	\*----------------------------------------------------------------*/
	$('.feed').on('append.infiniteScroll', function (event, response, path, items) {
		$(items).find('img[srcset]').each(function (i, img) {
			img.outerHTML = img.outerHTML;
		});
	});
	$('.feed').infiniteScroll({
		path: '.next.page-numbers',
		append: '.preview',
		checkLastPage: true,
		status: '.page-load-status',
	});
	/*----------------------------------------------------------------*\
		TESTIMONIALS MASONRY
	\*----------------------------------------------------------------*/
	var $grid = $('.feed-testimonial').masonry({
		itemSelector: '.preview-testimonial',
		columnWidth: '.preview-testimonial',
		percentPosition: true,
		visibleStyle: {
			transform: 'translateY(0)',
			opacity: 1
		},
		hiddenStyle: {
			transform: 'translateY(100px)',
			opacity: 0
		},
	});
	var msnry = $grid.data('masonry');
	$('.feed-testimonial').infiniteScroll({
		path: '.next.page-numbers',
		append: '.preview',
		checkLastPage: true,
		status: '.page-load-status',
		outlayer: msnry,
	});
	/*----------------------------------------------------------------*\
		UTILITY
	\*----------------------------------------------------------------*/
	$('.utility button').click(function () {
		$('.utility form').addClass('is-active');
		$('.utility form input').focus();
	});
	$('html').click(function (event) {
		if ($(event.target).closest('.utility button, .utility form').length === 0) {
			$('.utility form').removeClass("is-active");
		}
	});
	/*----------------------------------------------------------------*\
		MOBILE NAV
	\*----------------------------------------------------------------*/
	$('button.nav-toggle').click(function () {
		$('nav.hamburger').addClass('is-active');
	});
	$('html, body').click(function (event) {
		if ($(event.target).closest('button.nav-toggle, nav.hamburger').length === 0) {
			$('nav.hamburger').removeClass("is-active");
		}
	});
	/*----------------------------------------------------------------*\
		FIXED NAV
	\*----------------------------------------------------------------*/
	function defaultnav() {
		if ($('.navigation-block').hasClass('has-image')) {
			$('.navigation-block').addClass('is-transparent').removeClass('is-fixed').css('top', 'auto');
		} else {
			$('.navigation-block').removeClass('is-fixed').css('top', 'auto');
		}
		if ($('nav.hamburger').hasClass('has-image')) {
			$('nav.hamburger').addClass('is-transparent').removeClass('is-fixed').css('top', 'auto');
		} else {
			$('nav.hamburger').removeClass('is-fixed').css('top', 'auto');
		}
	}
	$(window).scroll(function () {
		var scroll = $(window).scrollTop();

		if (scroll >= 300) {
			$('.notification').addClass('is-closed');
			$('.navigation-block').removeClass('is-transparent').addClass('is-fixed').css('top', '0px');
		} else if (scroll > 200 && scroll < 300) {
			$('.navigation-block').css('top', '-300px');
		} else {
			if (scroll < position) {
				$('.navigation-block').css('top', '-300px');
				setTimeout(defaultnav, 1000);
			}
			var position = scroll;
			if ($('.navigation-block').hasClass('has-image')) {
				$('.navigation-block').addClass('is-transparent').removeClass('is-fixed').css('top', 'auto');
			} else {
				$('.navigation-block').removeClass('is-fixed').css('top', 'auto');
			}
		}
	});
	/*----------------------------------------------------------------*\
		WISHLIST URL
	\*----------------------------------------------------------------*/
	if ($('a.twitter').length) {
		var wishlist_url = $('a.twitter').attr('href');
		var wishlistURL = decodeURIComponent(wishlist_url.substring(wishlist_url.indexOf("=") + 1, wishlist_url.indexOf("&")));
		var form_url = '/get-a-quote/?website=' + wishlistURL;
		$(".page-title p a:first-child").attr("href", form_url);
		$(".banner a.button").attr("href", form_url);
	}
});