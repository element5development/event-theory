var $ = jQuery;

$(window).on("load", function () {
	$(".gallery-items img").each(function () {
		var width = $(this)[0].naturalWidth;
		var height = $(this)[0].naturalHeight;
		var grow = width * 100 / height;
		var basis = width * 240 / height;
		var pad = height / width * 100;
		var padding = pad + '%';
		$(this).siblings('i').css('padding-bottom', padding);
		$(this).parent('figure').css('flex-grow', grow).css('flex-basis', basis);
	});
});


$(document).ready(function () {

	/*----------------------------------------------------------------*\
			GALLERIES
	\*----------------------------------------------------------------*/
	$('.gallery-items .gallery-item').featherlightGallery({
		previousIcon: '«',
		nextIcon: '»',
		galleryFadeIn: 100,
		galleryFadeOut: 300
	});
});