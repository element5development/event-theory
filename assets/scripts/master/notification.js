var $ = jQuery;

$(document).ready(function () {
	/*----------------------------------------------------------------*\
		OPEN AND CLOSE
	\*----------------------------------------------------------------*/
	$('.notification button').click(function () {
		setCookie('show_cookie_message', 'no');
		$('.notification').addClass('is-closed');
	});
	$(window).scroll(function () {
		var scroll = $(window).scrollTop();
		if (scroll >= 300) {
			setCookie('show_cookie_message', 'no');
			$('header.page-title').removeClass('has-extra-spacing');
		}
	});
	/*----------------------------------------------------------------*\
		ADD ADDITIONAL SPACING IF NOTIFICATION IS DISPLAYED
	\*----------------------------------------------------------------*/
	var hiddenNote = getCookie("show_cookie_message");
	if (hiddenNote == 'no') {
		//nothing
	} else {
		$('header.page-title').addClass('has-extra-spacing');
	}
	/*----------------------------------------------------------------*\
		COOKIE
	\*----------------------------------------------------------------*/
	function setCookie(cookie_name, value) {
		var exdate = new Date();
		exdate.setDate(exdate.getDate() + (365 * 25));
		document.cookie = cookie_name + "=" + escape(value) + "; expires=" + exdate.toUTCString() + "; path=/";
	}

	function getCookie(cookie_name) {
		if (document.cookie.length > 0) {
			cookie_start = document.cookie.indexOf(cookie_name + "=");
			if (cookie_start != -1) {
				cookie_start = cookie_start + cookie_name.length + 1;
				cookie_end = document.cookie.indexOf(";", cookie_start);
				if (cookie_end == -1) {
					cookie_end = document.cookie.length;
				}
				return unescape(document.cookie.substring(cookie_start, cookie_end));
			}
		}
		return "";
	}

});