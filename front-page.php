<?php 
/*----------------------------------------------------------------*\

	HOME/FRONT PAGE TEMPLATE
	Customized home page commonly composed of various reuseable sections.

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/elements/navigation-mobile'); ?>

<?php get_template_part('template-parts/sections/headers/header'); ?>

<main id="main-content">
	<article>
		<section class="cta-banner">
			<?php while ( have_rows('ctas') ) : the_row(); ?>
				<?php 
					$bg = get_sub_field('background'); 
					$link = get_sub_field('link');
				?>
				<a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>" class="cta" style="background-image: url('<?php echo $bg['sizes']['medium'] ?>');">
					<h4><?php echo $link['title']; ?></h4>
					<div class="button is-white">view</div>
				</a>
			<?php endwhile; ?>
		</section>
		<section class="buttons">
			<h3>Our Capabilities</h3>
			<div>
				<?php while ( have_rows('capabilities') ) : the_row(); ?>
					<?php $button = get_sub_field('button'); ?>
					<a class="button is-white" href="<?php echo $button['url']; ?>" target="<?php echo $button['target']; ?>"><?php echo $button['title']; ?></a>
				<?php endwhile; ?>
			</div>
			<a class="button" href="/capabilities/">View All</a>
		</section>
		<section class="showrooms">
			<?php while ( have_rows('showrooms') ) : the_row(); ?>
				<?php 
					$bg = get_sub_field('background'); 
					$link = get_sub_field('link');
				?>
				<a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>" class="cta" style="background-image: url('<?php echo $bg['sizes']['medium'] ?>');">
					<h4><?php echo $link['title']; ?></h4>
					<div class="button is-white">view</div>
				</a>
			<?php endwhile; ?>
		</section>
		<?php
			if( have_rows('content') ):
				while ( have_rows('content') ) : the_row();

					if( get_row_layout() == 'wysiwyg' ):
						get_template_part('template-parts/sections/wysiwyg');
					elseif( get_row_layout() == 'wysiwyg_two_column' ): 
						get_template_part('template-parts/sections/wysiwyg-two');
					elseif( get_row_layout() == 'gallery' ): 
						get_template_part('template-parts/sections/gallery');
					elseif( get_row_layout() == 'videos' ): 
						get_template_part('template-parts/sections/videos');
					elseif( get_row_layout() == 'banner' ): 
						get_template_part('template-parts/sections/banner');
					elseif( get_row_layout() == 'buttons' ): 
						get_template_part('template-parts/sections/buttons');
					elseif( get_row_layout() == 'instagram' ): 
						get_template_part('template-parts/sections/instagram');
					endif;

				endwhile;
			endif; 
		?>
	</article>
</main>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>