<?php 
/*----------------------------------------------------------------*\

	DEFAULT SINGLE POST TEMPLATE
	More commonly only used for the default Blog/News post type.
	This is the page template for the post type, for the preview
	look under template-parts.

\*----------------------------------------------------------------*/
?>


<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/elements/navigation-mobile'); ?>

<?php get_template_part('template-parts/sections/headers/header'); ?>

<main id="main-content">

	<article>

		<section class="gallery">
			<div class="gallery-items">
				<?php $images = get_field('gallery'); ?>
				<?php foreach( $images as $image ): ?>
					<?php
						// PINTEREST BUTTON
						$website = get_site_url();
						$image_url =	$website . $image['sizes']['xlarge'];
						$pinterest = '<a target="_blank" class="pin-product" data-pin-do="buttonPin" data-pin-tall="true" data-pin-round="true" href="https://www.pinterest.com/pin/create/button/?url='.$website.'&media='.$image_url.'"><img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_round_red_32.png" /></a>';
					?>
					<figure>
						<?php echo $pinterest; ?>
						<div class="zoom">
							<svg>
								<use xlink:href="#search" />
							</svg>
						</div>
						<i></i>
						<img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
						<a class="gallery-item" href="<?php echo $image['url']; ?>"></a>
					</figure>
				<?php endforeach; ?>
			</div>
		</section>

	</article>
	
</main>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>