<?php

/*----------------------------------------------------------------*\

	CUSTOM MENU AREAS
	www.wp-hasty.com

\*----------------------------------------------------------------*/
function nav_creation() {
	$locations = array(
		'primary_nav' => __( 'Primary Navigation' ),
		'primary_nav_mobile' => __( 'Primary Mobile Navigation' ),
		'footer_nav' => __( 'Footer Navigation' ),
		'legal_nav' => __( 'Legal Navigation' )
	);
	register_nav_menus( $locations );
}
add_action( 'init', 'nav_creation' );

/*----------------------------------------------------------------*\
	ENABLE YOAST BREADCRUMBS
\*----------------------------------------------------------------*/
add_theme_support( 'yoast-seo-breadcrumbs' );
function jb_crumble_bread($link_text, $id) {
	$link_text = html_entity_decode($link_text);
	$crumb_length = strlen( $link_text );
 	$crumb_size = 14;
 	$crumble = substr( $link_text, 0, $crumb_size );
	if ( $crumb_length > $crumb_size ) {
		$crumble .= '...';
	}
	return $crumble;
}
add_filter('wp_seo_get_bc_title', 'jb_crumble_bread', 10, 2);

/*----------------------------------------------------------------*\
	CAPABILITIES
\*----------------------------------------------------------------*/
function siblings($link) {
	global $post;
	$siblings = get_pages('child_of='.$post->post_parent.'&parent='.$post->post_parent);
	foreach ($siblings as $key=>$sibling){
			if ($post->ID == $sibling->ID){
					$ID = $key;
			}
	}
	$page_before = '<a href="'.get_permalink($siblings[$ID-1]->ID).'">'.get_the_title($siblings[$ID-1]->ID).'</a>';
	$page_after = '<a href="'.get_permalink($siblings[$ID+1]->ID).'">'.get_the_title($siblings[$ID+1]->ID).'</a>';
	$closest = array('before'=>$page_before,'after'=>$page_after);

	if ($link == 'before' || $link == 'after') { 
		echo $closest[$link]; 
	} else { 
		return $closest; 
	}
}