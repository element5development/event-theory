<?php

/*----------------------------------------------------------------*\

	CUSTOM TAXONOMIES
	www.wp-hasty.com

\*----------------------------------------------------------------*/
// Taxonomy Key: format
function create_format_tax() {
	$labels = array(
		'name'              => _x( 'formats', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'format', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Search formats', 'textdomain' ),
		'all_items'         => __( 'All formats', 'textdomain' ),
		'parent_item'       => __( 'Parent format', 'textdomain' ),
		'parent_item_colon' => __( 'Parent format:', 'textdomain' ),
		'edit_item'         => __( 'Edit format', 'textdomain' ),
		'update_item'       => __( 'Update format', 'textdomain' ),
		'add_new_item'      => __( 'Add New format', 'textdomain' ),
		'new_item_name'     => __( 'New format Name', 'textdomain' ),
		'menu_name'         => __( 'format', 'textdomain' ),
	);
	$args = array(
		'labels' => $labels,
		'description' => __( '', 'textdomain' ),
		'hierarchical' => true,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_in_rest' => false,
		'show_tagcloud' => true,
		'show_in_quick_edit' => true,
		'show_admin_column' => false,
	);
	register_taxonomy( 'format', array('gallery', ), $args );
}
add_action( 'init', 'create_format_tax' );

add_action( 'pre_get_posts', function ( $query ) {
    if ( !is_admin() && $query->is_main_query() && $query->is_tax('format') ) {
      $query->set( 'posts_per_page', '6'   );
    }
});