<?php

/*----------------------------------------------------------------*\
	ADDITIONAL IMAGE SIZES
\*----------------------------------------------------------------*/
add_image_size( 'small', 400, 400 ); 
function small_image_sizes($sizes) {
	$sizes['small'] = __( 'Small' );
	return $sizes;
}
add_filter('image_size_names_choose', 'small_image_sizes');
add_image_size( 'xlarge', 2000, 1600 ); 
function large_image_sizes($sizes) {
	$sizes['xlarge'] = __( 'X-Large' );
	return $sizes;
}
add_filter('image_size_names_choose', 'large_image_sizes');

/*----------------------------------------------------------------*\
	ADDITIONAL FILE FORMATS
\*----------------------------------------------------------------*/
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  $mimes['zip'] = 'application/zip';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

add_filter('wp_check_filetype_and_ext', function($values, $file, $filename, $mimes) {
	if ( extension_loaded( 'fileinfo' ) ) {
		// with the php-extension, a CSV file is issues type text/plain so we fix that back to 
		// text/csv by trusting the file extension.
		$finfo     = finfo_open( FILEINFO_MIME_TYPE );
		$real_mime = finfo_file( $finfo, $file );
		finfo_close( $finfo );
		if ( $real_mime === 'text/plain' && preg_match( '/\.(csv)$/i', $filename ) ) {
			$values['ext']  = 'csv';
			$values['type'] = 'text/csv';
		}
	} else {
		// without the php-extension, we probably don't have the issue at all, but just to be sure...
		if ( preg_match( '/\.(csv)$/i', $filename ) ) {
			$values['ext']  = 'csv';
			$values['type'] = 'text/csv';
		}
	}
	return $values;
}, PHP_INT_MAX, 4);

/*----------------------------------------------------------------*\
	ENABLE TAGS FOR MEDIA LIBRARY
\*----------------------------------------------------------------*/
function wptp_add_tags_to_attachments() {
	register_taxonomy_for_object_type( 'post_tag', 'attachment' );
}
add_action( 'init' , 'wptp_add_tags_to_attachments' );