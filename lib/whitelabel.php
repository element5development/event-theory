<?php

/*----------------------------------------------------------------*\
	REMOVE UNNEEDED DASHBOARD WIDGETS
\*----------------------------------------------------------------*/
function remove_dashboard_widgets() {
	global $wp_meta_boxes;
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_drafts']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
}
add_action('wp_dashboard_setup', 'remove_dashboard_widgets' );
/*----------------------------------------------------------------*\
	DEVELOPER DASHBOARD WIDGET
\*----------------------------------------------------------------*/
function my_custom_dashboard_widgets() {
	global $wp_meta_boxes;
	wp_add_dashboard_widget('custom_help_widget', 'Theme Support', 'custom_dashboard_help');
}
function custom_dashboard_help() {
echo '<p>When the time comes you need to update your site or have some question on how to use WordPress or just confused feel free to contact us.<br><a href="mailto:support@element5digital.com">support@element5digital.com</a><br><a href="tel:+12485301000">(248) 530-1000</a><br><a target="_blank" href="https://element5digital.com/">element5digital.com</a><img src="/wp-content/themes/starting-point/dist/images/element5-logo.svg" alt="Element5 Digital" />';
}
add_action('wp_dashboard_setup', 'my_custom_dashboard_widgets');
/*----------------------------------------------------------------*\
	CUSTOM CSS FOR WP ADMIN AREA
\*----------------------------------------------------------------*/
function my_custom_admin() {
  echo '
		<style>
		.kaui div, 
		.kaui #wpadminbar div { 
			font-family: inherit; 
		}
		.kaui #adminmenuback, .kaui #adminmenu, 
		.kaui #adminmenuwrap {
			background: #fff;
		}
		.kaui #adminmenu>li>a, 
		.kaui #adminmenu div.wp-menu-image:before {
			color: #777;
		}
		.kaui #adminmenu li.menu-top>a:focus, 
		.kaui #adminmenu li a:hover, 
		.kaui #adminmenu li a:hover .wp-menu-image, 
		.kaui #adminmenu li.menu-top:hover, 
		.kaui #adminmenu li.opensub>a.menu-top, 
		.kaui #adminmenu li>a.menu-top:focus, 
		.kaui #adminmenu li a:focus div.wp-menu-image:before, 
		.kaui #adminmenu li.opensub div.wp-menu-image:before, 
		.kaui #adminmenu li:hover div.wp-menu-image:before {
			color: #000 !important;
		}
		.kaui #adminmenu>li>a.wp-first-item,
		.kaui #adminmenu ul.wp-submenu>li>a {
			color: #fff !important;
		}
		.kaui #adminmenu .wp-has-current-submenu .wp-submenu .wp-submenu-head, 
		.kaui #adminmenu li.wp-has-current-submenu.wp-menu-open a.wp-has-current-submenu {
			background: #1094c5 !important;
		}
		.kaui #adminmenu li#toplevel_page_kodeo-home {
			background: #4ebf51 !important;
		}
		.kaui #adminmenu .wp-submenu, 
		.kaui #adminmenu a.wp-has-current-submenu:focus+.wp-submenu {
			background: #34bbee !important;
			border-top: 1px solid #34bbee !important;
    	border-bottom: 1px solid #34bbee !important;
		}
		#wpadminbar .yoast-logo.svg,
		#wpadminbar .gforms-menu-icon.svg { 
			height: 100% !important; 
			background-position: center; 
		}
		li#wp-admin-bar-autoptimize .ab-item .ab-icon {	padding: 0 !important; }
		.js .control-section .accordion-section-title:focus, 
		.js .control-section .accordion-section-title:hover, 
		.js .control-section.open .accordion-section-title, 
		.js .control-section:hover .accordion-section-title,
		.metabox-holder-disabled .accordion-section-content, 
		.metabox-holder-disabled .accordion-section-title, 
		.metabox-holder-disabled .postbox {
			opacity: 1;
		}
		body.wp-admin div#TB_window.thickbox-loading {
			margin-left: 0 !important;
			width: 500px !important;
			height: 500px !important;
			max-width: 100% !important;
		}
		#custom_help_widget img {
			display: block;
			margin: 1em 1em 0 auto;
			width: 200px;
		}
		.rcl-container {
			padding-top: 35px;
		}
		.aiot-tree-headline {
			padding: 9px 10px 4px 10px !important;
		}
		.ant-spin-container > ul {
			padding-left: 18px !important;
		}
    </style>
  ';
}
add_action('admin_head', 'my_custom_admin');