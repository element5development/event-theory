<?php

/*----------------------------------------------------------------*\
	INIT WOOCOMMERCE
\*----------------------------------------------------------------*/
add_theme_support('woocommerce');
function yourtheme_setup() {
    add_theme_support( 'wc-product-gallery-lightbox' );
}
add_action( 'after_setup_theme', 'yourtheme_setup' );

/*----------------------------------------------------------------*\
	LOGOUT REDIRECT
\*----------------------------------------------------------------*/
function auto_redirect_after_logout(){
  wp_redirect( home_url() );
  exit();
}
add_action('wp_logout','auto_redirect_after_logout');

/*----------------------------------------------------------------*\
	LOGIN REDIRECT
\*----------------------------------------------------------------*/
function wc_custom_user_redirect( $redirect, $user ) {
	// Get the first of all the roles assigned to the user
	$role = $user->roles[0];
	$dashboard = admin_url();
	$myaccount = get_permalink( wc_get_page_id( 'myaccount' ) );
	$wishlist = get_permalink(414);
	if( $role == 'administrator' ) {
		$redirect = $wishlist;
	} elseif ( $role == 'shop-manager' ) {
		$redirect = $dashboard;
	} elseif ( $role == 'editor' ) {
		$redirect = $dashboard;
	} elseif ( $role == 'author' ) {
		$redirect = $dashboard;
	} elseif ( $role == 'customer' || $role == 'subscriber' ) {
		$redirect = $wishlist;
	} else {
		$redirect = $wishlist;
	}
	return $redirect;
}
add_filter( 'woocommerce_login_redirect', 'wc_custom_user_redirect', 10, 2 );

/*----------------------------------------------------------------*\
	REMOVE WOOCOMMERCE STYLESHEET
\*----------------------------------------------------------------*/
add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

/*----------------------------------------------------------------*\
	REMOVE BREADCRUMBS
\*----------------------------------------------------------------*/
remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);

/*----------------------------------------------------------------*\
	REMOVE SIDEBAR FROM ARCHIVE
\*----------------------------------------------------------------*/
remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10, 0);

/*----------------------------------------------------------------*\
	REMOVE SORTING DROPDOWN
\*----------------------------------------------------------------*/
remove_action( 'woocommerce_before_shop_loop' , 'woocommerce_catalog_ordering', 30 );

/*----------------------------------------------------------------*\
	REMOVE RESULT COUNT
\*----------------------------------------------------------------*/
remove_action( 'woocommerce_before_shop_loop' , 'woocommerce_result_count', 20 );

/*----------------------------------------------------------------*\
	REMOVE RELATED PRODUCTS
\*----------------------------------------------------------------*/
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

/*----------------------------------------------------------------*\
	REMOVE PRODUCT TABS
\*----------------------------------------------------------------*/
function woo_remove_product_tabs( $tabs ) {
	unset( $tabs['description'] ); // Remove the description tab
	unset( $tabs['reviews'] ); // Remove the reviews tab
	unset( $tabs['additional_information'] ); // Remove the additional information tab
	return $tabs;
}
add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

/*----------------------------------------------------------------*\
	REMOVE CATEGORIES FROMS SINGLE PRODUCT
\*----------------------------------------------------------------*/
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

/*----------------------------------------------------------------*\
	MOVE PRODUCT DESCRIPTION
\*----------------------------------------------------------------*/
function woocommerce_template_product_description() {
	wc_get_template( 'single-product/tabs/description.php' );
}
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_product_description', 20 );

/*----------------------------------------------------------------*\
	MOVE SHORT DESCRIPTION DESCRIPTION
\*----------------------------------------------------------------*/
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 2 );

/*----------------------------------------------------------------*\
	ADD LOGIN BUTTON
\*----------------------------------------------------------------*/
function login_user_feature(){
	echo '<a href="'.get_the_permalink(280).'" class="button login-btn">Login to Add to Wishlist</a>';
}
add_action( 'woocommerce_single_product_summary', 'login_user_feature', 25 );

/*----------------------------------------------------------------*\
	ADD GALLERY LIGHTBOX
\*----------------------------------------------------------------*/
add_theme_support( 'wc-product-gallery-lightbox' );

// /*----------------------------------------------------------------*\
// 	ADD PREVIOUS AND NEXT BUTTONS
// \*----------------------------------------------------------------*/
// function prev_next_product(){
// 		echo '<div class="prev_next_buttons">';
//     // 'product_cat' will make sure to return next/prev from current category
// 		// $previous = previous_post_link('%link', 'Previous Product', TRUE, ' ', 'product_cat');
// 		// echo $previous;
// 		// $terms = wp_get_post_terms( get_the_id(), 'product_cat', array( 'include_children' => false ) );
// 		// $term = reset($terms);
// 		// $term_link =  get_term_link( $term->term_id, 'product_cat' );  
// 		// echo '<a class="back" href="'.$term_link.'">All '. $term->name .'</a>';
// 		// $next = next_post_link('%link', 'Next Product', TRUE, ' ', 'product_cat');
// 		// echo $next;  
// 			$previous = previous_post_link('%link', 'PREVIOUS', TRUE, ' ', 'product_cat');
// 			$next = next_post_link('%link', 'NEXT', TRUE, ' ', 'product_cat');
// 			echo $previous;
// 			echo $next;
// 		echo '</div>';
// }
// add_action( 'woocommerce_after_single_product', 'prev_next_product' );
/*----------------------------------------------------------------*\
	ADD PRICE LABEL
\*----------------------------------------------------------------*/
function custom_price_message( $price ) {
	global $post;
	$terms = wp_get_post_terms( $post->ID, 'product_cat' );
	foreach ( $terms as $term ) $categories[] = $term->slug;

	if ( $price && get_field('price_description') == 'blank' ) {
		return $price;
	} else if ( $price && get_field('price_description') ) {
		$textafter = get_field('price_description');
		return $price . '<span class="price-description"> ' . $textafter . '</span>';
	} else if ( $price ) {
		$textafter = 'each'; 
		return $price . '<span class="price-description"> ' . $textafter . '</span>';
	} else {
		return $price;
	} 
}
add_filter( 'woocommerce_get_price_html', 'custom_price_message' );
/*----------------------------------------------------------------*\
	PRODUCT IMAGE ALT TEXT TO PRODUCT TITLE
\*----------------------------------------------------------------*/
function change_attachement_image_attributes( $attr, $attachment ){
    $parent = get_post_field( 'post_parent', $attachment);
    $type = get_post_field( 'post_type', $parent);
    if( $type != 'product' ){
        return $attr;
    }
    $title = get_post_field( 'post_title', $parent);
    $attr['alt'] = $title;
    $attr['title'] = $title;
    return $attr;
}
add_filter('wp_get_attachment_image_attributes', 'change_attachement_image_attributes', 20, 2);

/*----------------------------------------------------------------*\
	SRCSET REMOVED -- BREAKS INFINTE SCROLL ON SAFARI
\*----------------------------------------------------------------*/
function meks_disable_srcset( $sources ) {
	return false;
}
add_filter( 'wp_calculate_image_srcset', 'meks_disable_srcset' );

/*----------------------------------------------------------------*\
	PRODUCT ARCHIVE rREMOVE DEFAULT IAMGE AND ADD ACF IMAGE
\*----------------------------------------------------------------*/
remove_action('woocommerce_before_shop_loop_item_title','woocommerce_template_loop_product_thumbnail',10);
function custom_field_display_below_title(){
	global $product;
	if ( $image_url = get_field( 'preview_image', $product->get_id() ) ) {
		echo '<img src="' . $image_url['sizes']['small'] . '" alt="'.get_the_title( $product->get_id() ).'" />';
	} else {
		echo '<img src="' . get_the_post_thumbnail_url( $product->get_id() ) . '" alt="'.get_the_title( $product->get_id() ).'" />';
	}
}
add_action( 'woocommerce_after_shop_loop_item_title', 'custom_field_display_below_title', 2 );