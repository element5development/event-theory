<?php

/*----------------------------------------------------------------*\

	CUSTOM WIDGET AREAS
	www.wp-hasty.com

\*----------------------------------------------------------------*/

function notification_sidebar() {
	$args = array(
		'name'          => __( 'Notification Area' ),
		'id'            => 'notification',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widgettitle">',
		'after_title'   => '</h3>',
	);
	register_sidebar($args);
}
add_action( 'widgets_init', 'notification_sidebar' );

function address_sidebar() {
	$args = array(
		'name'          => __( 'Address Area' ),
		'id'            => 'address',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widgettitle">',
		'after_title'   => '</h3>',
	);
	register_sidebar($args);
}
add_action( 'widgets_init', 'address_sidebar' );

function social_sidebar() {
	$args = array(
		'name'          => __( 'Social Area' ),
		'id'            => 'social',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widgettitle">',
		'after_title'   => '</h3>',
	);
	register_sidebar($args);
}
add_action( 'widgets_init', 'social_sidebar' );