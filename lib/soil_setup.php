<?php

/*----------------------------------------------------------------*\

	SOIL SETUP
	cleans up standard wordpress fuctions such as class names

\*----------------------------------------------------------------*/
add_theme_support('soil-clean-up');
add_theme_support('soil-nav-walker');
add_theme_support('soil-relative-urls');