<?php 
/*----------------------------------------------------------------*\

	GALLERY ARCHIVES BY FORMAT

\*----------------------------------------------------------------*/
?>


<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/elements/navigation-mobile'); ?>

<?php $taxonomy = get_queried_object(); ?>

<header class="page-title">
	<section>

		<h1><?php echo $taxonomy->name; ?></h1>

	</section>
</header>

<main id="main-content">
	<article> 
		<?php if ( have_posts() ) : ?>
			<section class="feed feed-post feed-gallery">
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part('template-parts/elements/previews/preview-gallery'); ?>
				<?php endwhile; ?>
			</section>
			<section class="infinite-scroll is-standard-width has-small-spacing">
				<div class="page-load-status">
					<p class="infinite-scroll-request">
						<svg class="loading" x="0px" y="0px" width="40px" height="40px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;">
							<path d="M43.935,25.145c0-10.318-8.364-18.683-18.683-18.683c-10.318,0-18.683,8.365-18.683,18.683h4.068c0-8.071,6.543-14.615,14.615-14.615c8.072,0,14.615,6.543,14.615,14.615H43.935z">
								<animateTransform attributeType="xml"
									attributeName="transform"
									type="rotate"
									from="0 25 25"
									to="360 25 25"
									dur="0.6s"
									repeatCount="indefinite"/>
							</path>
						</svg>
					</p>
					<p class="infinite-scroll-last"></p>
					<p class="infinite-scroll-error"></p>
				</div>
				<?php the_posts_pagination( array(
					'prev_text'	=> __( 'Previous page' ),
					'next_text'	=> __( 'Next page' ),
				) ); ?>
			</section>
		<?php else : ?>
			<!-- NO RESULTS FOUND -->
		<?php endif; ?>
	</article>
</main>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>