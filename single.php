<?php 
/*----------------------------------------------------------------*\

	DEFAULT SINGLE POST TEMPLATE
	More commonly only used for the default Blog/News post type.
	This is the page template for the post type, for the preview
	look under template-parts.

\*----------------------------------------------------------------*/
?>


<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/elements/navigation-mobile'); ?>

<?php get_template_part('template-parts/sections/headers/header-post'); ?>

<nav class="breadcrumbs">
	<?php if ( function_exists('yoast_breadcrumb') ) { 
		yoast_breadcrumb('<nav id="breadcrumbs">','</nav>'); 
	} ?>
</nav>

<main id="main-content">

	<article> 
		<?php if ( get_field('featured_img') ) : ?>
			<section class="featured-img">
				<?php $image = get_field('featured_img'); ?>
				<img class="featured-img" src="<?php echo $image['sizes']['xlarge']; ?>" alt="<?php echo $image['alt']; ?>" />
			</section>
		<?php endif; ?>
		<?php if ( '' !== get_post()->post_content ) : ?>
			<section class="wysiwyg-block">
				<?php the_content(); ?>
			</section>
		<?php endif; ?>
		<?php get_template_part('template-parts/elements/share'); ?>
	</article>

</main>

<?php get_template_part('template-parts/sections/related'); ?>
<?php if ( comments_open() || get_comments_number() ) : ?>
	<?php comments_template(); ?>
<?php endif; ?>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>