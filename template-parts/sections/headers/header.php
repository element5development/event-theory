<?php 
/*----------------------------------------------------------------*\

	HEADER WITH IMAGE BACKGROUND

\*----------------------------------------------------------------*/
?>


<header class="page-title <?php if ( get_field('title_bg_img') ) : ?>has-image<?php endif; ?>" style="background-image: url('<?php the_field('title_bg_img'); ?>');">
	<section>

		<h1>
			<?php 
				if ( get_field('page_title') ) :
					the_field('page_title');
				else :
					the_title();
				endif;
			?>
		</h1>

		<?php if ( get_field('title_description') ) : ?>
			<p>
				<?php the_field('title_description'); ?>
			</p>
		<?php endif; ?>

	</section>
	<div class="overlay"></div>
</header>