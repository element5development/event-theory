<?php 
/*----------------------------------------------------------------*\

	INSTAGRAM FEED

\*----------------------------------------------------------------*/
?>


<section class="instagram">
	<h3>Follow Us on Instagram</h3>
	<?php echo do_shortcode('[instagram-feed]'); ?>
</section>