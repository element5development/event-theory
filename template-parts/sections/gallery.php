<?php 
/*----------------------------------------------------------------*\

	IMAGE GALLERY WITH LIGHTBOX ZOOM

\*----------------------------------------------------------------*/
?>

<section class="gallery">
	<?php if ( get_sub_field('title') ) : ?>
		<h2><?php the_sub_field('title'); ?></h2>
	<?php endif; ?>
	<div class="gallery-items">
		<?php $images = get_sub_field('gallery'); ?>
		<?php foreach( $images as $image ): ?>
			<?php
				// PINTEREST BUTTON
				$website = get_site_url();
				$image_url =	$website . $image['sizes']['xlarge'];
				$pinterest = '<a target="_blank" class="pin-product" data-pin-do="buttonPin" data-pin-tall="true" data-pin-round="true" href="https://www.pinterest.com/pin/create/button/?url='.$website.'&media='.$image_url.'"><img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_round_red_32.png" /></a>';
			?>
			<figure>
				<?php echo $pinterest; ?>
				<div class="zoom">
					<svg>
						<use xlink:href="#search" />
					</svg>
				</div>
				<i></i>
				<img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
				<a class="gallery-item" href="<?php echo $image['url']; ?>"></a>
			</figure>
		<?php endforeach; ?>
	</div>
</section>