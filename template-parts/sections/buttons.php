<?php 
/*----------------------------------------------------------------*\

	BUTTONS

\*----------------------------------------------------------------*/
?>


<section class="buttons">
	<?php if ( get_sub_field('title') ) : ?>
		<h3><?php the_sub_field('title'); ?></h3>
	<?php endif; ?>
	<div>
		<?php while ( have_rows('buttons') ) : the_row(); ?>
			<?php $button = get_sub_field('button'); ?>
			<a class="button is-white" href="<?php echo $button['url']; ?>" target="<?php echo $button['target']; ?>"><?php echo $button['title']; ?></a>
		<?php endwhile; ?>
	</div>
	<?php if ( get_sub_field('cta') ) : ?>
		<?php $button = get_sub_field('cta'); ?>
		<a class="button" href="<?php echo $button['url']; ?>" target="<?php echo $button['target']; ?>"><?php echo $button['title']; ?></a>
	<?php endif; ?>
</section>