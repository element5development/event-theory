<?php 
/*----------------------------------------------------------------*\

	IMAGE GALLERY WITH LIGHTBOX ZOOM

\*----------------------------------------------------------------*/
?>

<section class="wysiwyg-block two-col">
	<?php if ( get_sub_field('headline') ) : ?>
		<h3><?php the_sub_field('headline'); ?></h3>
	<?php endif; ?>
	<div>
		<?php the_sub_field('left_wysiwyg'); ?>
	</div>
	<div>
		<?php the_sub_field('right_wysiwyg'); ?>
	</div>
</section>