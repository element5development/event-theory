<?php 
/*----------------------------------------------------------------*\

	FULL WIDTH BANNER

\*----------------------------------------------------------------*/
?>

<?php $bg = get_sub_field('background'); ?>
<section class="banner" style="background-image: url('<?php echo $bg['sizes']['xlarge']; ?>');">
	<div>
		<h2><?php the_sub_field('title'); ?></h2>
		<?php $link = get_sub_field('button'); ?>
		<?php if( $link ): ?>
			<a class="button is-ghost is-white" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
		<?php endif; ?>
	</div>
</section>