<?php 
/*----------------------------------------------------------------*\

	VIDEO GALLERY WITH LIGHTBOX PLAY

\*----------------------------------------------------------------*/
?>

<section class="video-gallery">
	<?php if ( get_sub_field('title') ) : ?>
		<h2><?php the_sub_field('title'); ?></h2>
	<?php endif; ?>
	<div class="videos">
		<?php while ( have_rows('videos') ) : the_row(); ?>
			<a class="gallery-item" href="https://www.youtube.com/embed/<?php the_sub_field('youtube_id') ?>?rel=0&amp;showinfo=0" data-featherlight="iframe" data-featherlight-iframe-width="960" data-featherlight-iframe-height="540">
				<div style="background-image: url('https://img.youtube.com/vi/<?php the_sub_field('youtube_id') ?>/hqdefault.jpg');">
					<div>
						<svg>
							<use xlink:href="#play" />
						</svg>
					</div>
				</div>
			</a>
		<?php endwhile; ?>
	</div>
</section>