<?php 
/*----------------------------------------------------------------*\

	DEFAULT FOOTER
	Made up of widget areas
	May need to add or remove additional areas depending on the design

\*----------------------------------------------------------------*/
?>

<footer>
	<section>
		<div>
			<?php dynamic_sidebar( 'address' ); ?>
		</div>
		<div>
			<nav>
				<?php wp_nav_menu(array( 'theme_location' => 'footer_nav' )); ?>
			</nav>
		</div>
		<div>
			<?php dynamic_sidebar( 'social' ); ?>
		</div>
	</section>
	<div class="copyright">
		<section class="legal">
			<p>©<?php echo date('Y'); ?> <?php echo get_bloginfo( 'name' ); ?>. All Rights Reserved.</p>
			<nav>
				<?php wp_nav_menu(array( 'theme_location' => 'legal_nav' )); ?>
			</nav>
		</section>
	</div>
</footer>