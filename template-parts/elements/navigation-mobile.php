<?php 
/*----------------------------------------------------------------*\

	PRIMARY NAVIGATION
	Most commonly contains all the top level pages and search options.

\*----------------------------------------------------------------*/
?>

<div class="navigation-block-mobile
	<?php if ( get_field('title_bg_img') ) : ?> 
		has-image is-transparent
	<?php endif; ?>
">
	<?php get_template_part('template-parts/elements/notification'); ?>
	<a href="<?php echo get_home_url(); ?>">
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 80">
			<path d="M43.15 44.71H20.9v14.36h24.86v11.21H7.28V9.82h37.23V21H20.9v12.58h22.25zM64.57 21.19H49.32V9.72h43.4v11.47H77.3v49H64.57z"/>
		</svg>
	</a>
	<button class="nav-toggle">
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 28 28">
			<path d="M0 23.33V20.2h28v3.13zm0-7.8v-3.06h28v3.06zM0 4.67h28V7.8H0z" />
		</svg>
	</button>
</div>

<nav class="hamburger">
	<a href="<?php echo get_home_url(); ?>">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/event-theory-logo-color.svg" alt="Event Theory Logo">
	</a>
	<form role="search" method="get" action="<?php echo get_site_url(); ?>">
		<input type="search" placeholder="<?php echo esc_attr_x( 'Search …', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
		<button type="submit" class="is-small is-borderless">
			<svg>
				<use xlink:href="#search" />
			</svg>
		</button>
	</form>
	<?php wp_nav_menu(array( 'theme_location' => 'primary_nav_mobile' )); ?>
	<div class="location">
		<span>Warren:</span>
		<a href="tel:+15867550000" class="button is-ghost">
			586.755.0000
		</a>
	</div>
	<div class="location">
		<span>Grand Rapids:</span>
		<a href="tel:+16168187300" class="button is-ghost">
			616.818.7300
		</a>
	</div>
	<div class="location">
		<span>Traverse City:</span>
		<a href="tel:+12312259261" class="button is-ghost">
			231.225.9261
		</a>
	</div>
</nav>

<nav class="is-bottom">
	<a class="button is-white" href="<?php the_permalink(402); ?>">Collection</a>
	<a class="button is-white" href="<?php the_permalink(283); ?>">Get a Quote</a>
	<a class="button is-white" href="<?php the_permalink(282); ?>">Contact</a>
</nav>