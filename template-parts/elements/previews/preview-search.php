<?php 
/*----------------------------------------------------------------*\

	PREVIEW ELEMENT FOR SEARCH RESULT

\*----------------------------------------------------------------*/
?>

<article class="preview preview-search">
	<a href="<?php the_permalink(); ?>"></a>
	<?php if ( get_post_type() === 'product' ) : ?>
		<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'small' ); ?>
		<img src="<?php  echo $image[0]; ?>" alt="<?php the_title(); ?>" />
	<?php elseif ( get_field('featured_image') ) : ?>
		<?php $image = get_field('featured_image'); ?>
		<img src="<?php echo $image['sizes']['small']; ?>" alt="<?php the_title(); ?>" />
	<?php elseif ( get_field('title_bg_img') ) : ?>
		<img src="<?php the_field('title_bg_img'); ?>" alt="<?php the_title(); ?>" />
	<?php endif; ?>
	<div>
		<h3>
			<?php 
				if ( get_field('page_title') ) :
					the_field('page_title');
				else :
					the_title();
				endif;
			?>
		</h3>
		<?php the_excerpt(); ?><span class="more">read more</span>
	</div>
</article>
