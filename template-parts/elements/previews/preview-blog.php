<?php 
/*----------------------------------------------------------------*\

	PREVIEW ELEMENT FOR BLOG/NEWS POSTS

\*----------------------------------------------------------------*/
?>

<article class="preview preview-post">
	<a href="<?php the_permalink(); ?>"></a>
	<div class="featured-image">
		<?php $image = get_field('featured_img'); ?>
		<img src="<?php echo $image['sizes']['small']; ?>" alt="<?php echo $image['alt']; ?>" />
	</div>
	<h1><?php the_title(); ?></h1>
	<p><?php echo get_excerpt(250); ?></p>
	<div class="buttons">
		<div class="button is-fluid">Read More</div>
	</div>
</article>
