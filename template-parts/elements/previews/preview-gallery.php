<?php 
/*----------------------------------------------------------------*\

	PREVIEW ELEMENT FOR BLOG/NEWS POSTS

\*----------------------------------------------------------------*/
?>

<article class="preview preview-gallery">
	<?php $image = get_field('featured_image'); ?>
	<div class="featured" style="background-image: url(<?php echo $image['sizes']['small']; ?>);"></div>
	<p>View</p>
	<a href="<?php the_permalink(); ?>"></a>
</article>
