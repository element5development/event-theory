<?php 
/*----------------------------------------------------------------*\

	PREVIEW ELEMENT FOR BLOG/NEWS POSTS

\*----------------------------------------------------------------*/
?>

<article class="preview preview-testimonial">
	<?php the_content(); ?>
</article>
