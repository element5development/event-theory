<?php 
/*----------------------------------------------------------------*\

	PRIMARY NAVIGATION
	Most commonly contains all the top level pages and search options.

\*----------------------------------------------------------------*/
?>

<!-- // is_active_sidebar('notification') -->

<?php if ( $_COOKIE['show_cookie_message'] == 'no' ) : ?>
<?php else: ?>
	<?php if ( is_active_sidebar( 'notification' ) ) : ?>
		<div class="notification">
			<?php dynamic_sidebar( 'notification' ); ?>
			<button class="is-small is-borderless is-white ">Close</button>
		</div>
	<?php endif; ?>
<?php endif; ?>