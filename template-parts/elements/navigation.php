<?php 
/*----------------------------------------------------------------*\

	PRIMARY NAVIGATION
	Most commonly contains all the top level pages and search options.

\*----------------------------------------------------------------*/
?>

<div class="navigation-block
	<?php if ( get_field('title_bg_img') && !is_search() ) : ?> 
		has-image is-transparent
	<?php endif; ?>
">
	<?php get_template_part('template-parts/elements/notification'); ?>
	<nav class="utility">
		<ul>
			<li>
				<span>Warren:</span>
				<a href="tel:+15867550000" class="button is-ghost">
					586.755.0000
				</a>
			</li>
			<li>
				<span>Grand Rapids:</span>
				<a href="tel:+16168187300" class="button is-ghost">
					616.818.7300
				</a>
			</li>
			<li>
				<span>Traverse City:</span>
				<a href="tel:+12312259261" class="button is-ghost">
					231.225.9261
				</a>
			</li>
			<?php if ( is_user_logged_in() ) : ?>
				<li>
					<a href="<?php the_permalink(414) ?>" class="button is-ghost">
						WishList
					</a>
				</li>
			<?php else : ?>
				<li>
					<a href="<?php the_permalink(280) ?>" class="button is-ghost">
						Login
					</a>
				</li>
			<?php endif; ?>
			<li>
				<form role="search" method="get" action="<?php echo get_site_url(); ?>">
					<input type="search" placeholder="<?php echo esc_attr_x( 'Search …', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
					<button type="submit" class="is-small is-borderless">
						<svg>
							<use xlink:href="#search" />
						</svg>
					</button>
				</form>
				<button class="is-small is-borderless">
					<svg>
						<use xlink:href="#search" />
					</svg>
				</button>
			</li>
		</ul>
	</nav>
	<a href="<?php echo get_home_url(); ?>">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/event-theory-logo-color.svg" alt="Event Theory Logo">
	</a>
	<nav class="primary">
		<?php wp_nav_menu(array( 'theme_location' => 'primary_nav' )); ?>
	</nav>
</div>