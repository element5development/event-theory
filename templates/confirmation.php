<?php 
/*----------------------------------------------------------------*\

	Template Name: Confirmation
	
\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/elements/navigation-mobile'); ?>

<main>
	<article>
		<section class="wysiwyg-block">
			<img src="<?php echo get_template_directory_uri(); ?>/dist/images/confirmation.svg" alt="message recieved" />
			<h1><?php the_field('page_title'); ?></h1>
			<p><?php the_field('title_description'); ?></p>
			<?php if ( is_page(284) ) : ?>
				<p><strong>As a reminder, this is what you've sent us:</strong></p>
				<p>
					My name is <?php echo $_GET['fname']; ?> <?php echo $_GET['lname']; ?>.<br/>
					My email is <?php echo $_GET['email']; ?>.<br/>
					<?php if ( $_GET['phone'] ) : ?>
						My phone is <?php echo $_GET['phone']; ?>.<br/>
					<?php endif; ?>
						I am planning a <?php echo $_GET['type']; ?> for <?php echo $_GET['date']; ?>.<br/>
					<?php if ( $_GET['location'] ) : ?>
						The event will take place at the <?php echo $_GET['location']; ?>.<br/>
					<?php endif; ?>
					<?php if ( $_GET['budget'] ) : ?>
						My budget is currently at <?php echo $_GET['budget']; ?>.<br/>
					<?php endif; ?>
					<?php if ( $_GET['note'] ) : ?>
						Additional notes:<br/>
						<?php echo $_GET['note']; ?>
					<?php endif; ?>
				</p>
			<?php endif; ?>
		</section>
		<section class="buttons">
			<h3>Browse Our Products</h3>
			<div>
				<?php 
					$terms = get_terms( array( 'taxonomy' => 'product_cat', 'parent' => 0, 'exclude' => '115' )  );
				?>
				<?php foreach ( $terms as $term ) : ?>
					<a class="button is-white" href="<?php echo get_term_link($term); ?>"><?php echo $term->name ?></a>
				<?php endforeach; ?>
			</div>
		</section>
	</article>
</main>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>