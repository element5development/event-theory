<?php 
/*----------------------------------------------------------------*\

	Template Name: Gallery Categories
	
\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/elements/navigation-mobile'); ?>

<?php get_template_part('template-parts/sections/headers/header'); ?>

<main>

	<article>
		<div class="formats">
			<?php 
				$terms = get_terms( 'format' );
			?>
			<?php foreach ( $terms as $term ) : ?>
				<?php $image = get_field('featured_image', $term); ?>
				<a href="<?php echo get_term_link($term); ?>" class="format" style="background-image: url('<?php echo $image['sizes']['medium'] ?>');">
					<h2><?php echo $term->name ?></h2>
					<div class="button is-white">View Collection</div>
				</a>
			<?php endforeach; ?>
		</div>
	</article>
	
</main>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>