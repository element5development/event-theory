<?php 
/*----------------------------------------------------------------*\

	Template Name: Collections
	
\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/elements/navigation-mobile'); ?>

<?php get_template_part('template-parts/sections/headers/header'); ?>

<main>

	<article>
		<div class="formats">
			<?php 
				$terms = get_terms( array( 'taxonomy' => 'product_cat', 'parent' => 0, 'exclude' => '115' )  );
			?>
			<?php foreach ( $terms as $term ) : ?>
				<?php $image = get_field('featured_image', $term); ?>
				<a href="<?php echo get_term_link($term); ?>" class="format" style="background-image: url('<?php echo $image['sizes']['medium'] ?>');">
					<h2><?php echo $term->name ?></h2>
					<div class="button is-white">View Collection</div>
				</a>
			<?php endforeach; ?>
			<div class="format is-last">
				<div>
					<h2>Need some help with your event?</h2>
					<p>Let us know what you are looking for and we can help make your event a reality. </p>
					<a href="<?php echo get_permalink(282); ?>" class="button">Get Started</a>
				</div>
			</div>
		</div>
	</article>
	
</main>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>