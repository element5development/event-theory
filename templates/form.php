<?php 
/*----------------------------------------------------------------*\

	Template Name: Form
	
\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/elements/navigation-mobile'); ?>

<?php get_template_part('template-parts/sections/headers/header-form'); ?>

<main>

	<article class="for-form">

		<?php
			if( have_rows('content') ):
				while ( have_rows('content') ) : the_row();

					if( get_row_layout() == 'wysiwyg' ):
						get_template_part('template-parts/sections/wysiwyg');
					elseif( get_row_layout() == 'wysiwyg_two_column' ): 
						get_template_part('template-parts/sections/wysiwyg-two');
					elseif( get_row_layout() == 'gallery' ): 
						get_template_part('template-parts/sections/gallery');
					elseif( get_row_layout() == 'videos' ): 
						get_template_part('template-parts/sections/videos');
					elseif( get_row_layout() == 'banner' ): 
						get_template_part('template-parts/sections/banner');
					elseif( get_row_layout() == 'buttons' ): 
						get_template_part('template-parts/sections/buttons');
					elseif( get_row_layout() == 'instagram' ): 
						get_template_part('template-parts/sections/instagram');
					endif;

				endwhile;
			endif; 
		?>

	</article>
	
</main>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>